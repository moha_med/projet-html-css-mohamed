<html>
<head> 
    <meta charset="utf-8">
    <meta name="description" content="Ma 2eme page HTML">
    <meta name="keywords" content="HTML5, CSS3">
    <meta name="author" content="M0ha">
    <link rel="stylesheet" href="css/style.css"> 

</head>
    <body> 
        <ul>
            <li><a href="index.html">Accueil</a></li>
            <li><a href="regles.asp">Les règles</a></li>
            <li><a href="compet.asp">compétition</a></li>
            <li><a href="star.asp">les stars et legendes</a></li>
          </ul> 
        <h1> les règles du football</h1>
        <h2>le terrain de football</h2>
        <p class="c1">Le foot se joue sur un terrain rectangulaire qui mesure entre 90 et 120 mètres de long et 45 et 90 mètres de large.
            Le terrain est délimité par des lignes tracées : les deux lignes les plus longues sont les lignes de touche (longueurs du terrain), les deux plus courtes sont les lignes de but (largeurs du terrain). Les deux buts sont placés au centre de chaque ligne de but en fond de terrain. Ils sont entourés par une "surface de réparation" qui se situe à 16,5 m de l’intérieur de chaque montant du but et de la ligne de but. Le point de penalty est marqué à 11 m en face du but et à l’extérieur de chaque surface de réparation, un arc de cercle est tracé.
            Durée d'un match de football :

Un match se déroule en deux mi-temps de 45 minutes, séparées par une pause de 15 minutes.
Ce cas la est presents que si le match est un match pour se qualifier a une competition ou dans une competition:Si au terme de ces deux mi-temps les équipes sont à égalité, elles jouent alors les prolongations : ce sont deux nouvelles mi-temps de 15 minutes. S'il y a encore égalité, c'est l'épreuve des tirs au but qui les départagera : 5 joueurs de chaque équipe tirent à tour de rôle depuis le point de penalty.
        </p>
        <img src="images/terrain.jpg" >
        <h2>Les joueurs de football</h2>
        <p class="c1">Deux équipes de 11 joueurs dont un gardien de but (plus trois remplaçants) s'affrontent autour d'un ballon rond . Les matchs ne peuvent se disputer à moins de 7 joueurs par équipe (gardien compris).
            Les joueurs ne peuvent toucher le ballon ni avec les mains ni avec les bras.</p>
            <img src="images/equipe.jpg">
        <h2> Arbitrages </h2>
        <p class="c1"> Un arbitre central dirige le match. Il est assisté par deux de touche ou arbitres assistants qui signalent avec un drapeau les hors-jeu et hors lignes. Un autre arbitre peut assister également l'arbitre central par arbitrage vidéo dans le cadre des matchs importants.
            Selon la nature de la faute commise par un joueur ou le lieu de sortie du terrain de la balle, l'arbitre peut siffler des fautes :

    une rentrée de touche,
    un coup de pied de coin ou corner,
    un coup franc indirect,
    un coup franc direct, qui doit partir de l'endroit où la faute a été commise par l'adversaire,
    un penalty.
    Les fautes graves sont sanctionnées par l’arbitre central qui utilise deux sortes de cartons :

    le carton jaune : avertissement pour le joueur.
    le carton rouge : expulsion définitive du joueur. Il peut être donné à la suite d'un carton jaune ou directement si la faute est trop grave.
    <p>
    <img src="images/foot arbitre.jpg" >
        
        <h2> Le but d'un match de Football </h2>  
        <p class="c1"> Pour gagner, chaque équipe doit envoyer le ballon le plus de fois possible dans les buts de l'équipe adverse. Chaque but rapporte un point. L'équipe gagnante est celle qui a marqué le plus de buts contre l'équipe adverse.</p>
        <img src="images/sal nueve dans la maison.jpg" >
        
    </body>
    